data "ignition_file" "environment" {
  path = "/home/${var.username}/environment"
  content {
    content = templatefile("${path.module}/templates/environment", {
      conf = var.config
      aws_access_key_id = var.aws_access_key_id
      aws_secret_access_key = var.aws_secret_access_key
      aws_s3_bucket = var.aws_s3_bucket
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "send_container_unit" {
  path = "/home/${var.username}/.config/containers/systemd/send.container"
  content {
    content = templatefile(
      "${path.module}/templates/send.container",
      {
        username = var.username
      }
    )
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  files = [
    data.ignition_file.send_container_unit.rendered,
    data.ignition_file.environment.rendered,
  ]
}
