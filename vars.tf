variable "username" {
  type = string
}

variable "uid" {
  type = number
}

variable "gid" {
  type = number
}

variable "config" {
  type = object({
    base_url = string,
    max_expire = number,
    max_file_size = number,
    redis_host = string,
  })
}

variable "aws_access_key_id" {
  type = string
}

variable "aws_secret_access_key" {
  type = string
}

variable "aws_s3_bucket" {
  type = string
}
